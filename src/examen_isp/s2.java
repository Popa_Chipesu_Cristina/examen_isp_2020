package examen_isp;
import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import javax.swing.*;
	import java.util.*;
		
	public class s2 extends JFrame {

	    JLabel Text2, Text1;
	    JTextArea tArea1, tArea2;
	    JButton Copy;

	    s2() {


	        setTitle("Copiere");
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        init();
	        setSize(500, 500);
	        setVisible(true);
	    }

	    public void init() {

	        this.setLayout(null);
	        int height = 20;

	        Text1 = new JLabel("Text de copiat");
	        Text1.setBounds(10, 50, 250, height);
	        
	        Text2 = new JLabel("Text de salvat");
	        Text2.setBounds(10, 150, 250, height);

	        Copy = new JButton("Copiere");
	        Copy.setBounds(10, 250, 100, height);

	        Copy.addActionListener(new Button());

	        tArea1 = new JTextArea();
	        tArea1.setBounds(10, 100, 200, 20);
	        
	        tArea2 = new JTextArea();
	        tArea2.setBounds(10, 200, 200, 20);

	        add(Text1);
	        add(Text2);
	        add(Copy);
	        add(tArea1);
	        add(tArea2);

	    }

	    public static void main(String[] args) {
	        new s2();
	    }

	    class Button implements ActionListener {

	        public void actionPerformed(ActionEvent e) {
	            final String text1 = s2.this.tArea1.getText();
	    
                tArea2.setText(text1);
	   

	        }

	    }

	}

